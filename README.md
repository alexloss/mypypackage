# mypypackage

A squeleton python package which includes automation such as

---

## Pytest: unit testing & more

```sh
pip install "./mypypackage[develop]"
```

The tests can be found in the `mypypackage/tests/` folder

```sh
pytest ./mypypackage
```

---

## Sphinx: Documentation

```sh
pip install "./mypypackage[docs]"
```

Using Sphinx, you can compile the documentation for your package in the `docs/`folder

```sh
cd docs/
make html
firefox ./_build/html/index.html
```

### Troubleshooting

If you get the warning `Error loading plugin: lib<X>.so.<digit>: cannot open shared object file`, please try to install said library

E.g. `aspell`, `hspell`, `nuspell` and `libvoikko`

``` sh
# debian
sudo apt install aspell aspell-uk hspell nuspell libvoikko1
# arch
sudo pacman -S aspell aspell-uk hspell nuspell libvoikko
```

aspell`, `hspell`, `nuspell` and `libvoikko`



---

## Tox: Multi Python Automated testing

```sh
pip install "./mypypackage[develop]"
```

Tox allows to run different test systems for your package, including documentation spelling, testing your package with pytest in multiple python versions and run linting and formatting tests.

```sh
tox ./mypypackage
```

---

## Distribution

```sh
pip install "./mypypackage[admin]"
```

Distributing your package can be done with the twine package. It allows you to sync your package with Pypi or your private repository server such as devpi.

```sh
python3 bdist wheel ./devpi
twine upload -r devpi mypypackage*.whl
```
