#!/usr/bin/env python

def from_cli():
    """
    The default python binding of this package runs this function.

    Example:

        >>> from_cli()
        Hello from the command line!

    """
    print("Hello from the command line!")


if __name__ == "__main__":
    from_cli()
