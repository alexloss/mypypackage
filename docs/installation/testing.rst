Test environment
============================

You can install all required python packages using ``pip install "path/to/mypypackage[develop]"``

In order to run the ``tox`` test module in all the target python versions, you need to install them. By default this package tries to use python version 2.7, 3.6, 3.9 and python3 (whichever the default python3 binding points to).

If you do not have some of the python versions available in your package manager, then you might want to get them through a new ``ppa`` such as ``ppa:deadsnakes/ppa``

here is a tipo