CLI usage
=========

The responsibilities for the command line handling is in the ``mypypackage.cli`` module.

.. automodule:: mypypackage.cli
   :members:
