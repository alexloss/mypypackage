# Required to run tests 
pytest
# Required to run pytest on multiple cores `pytest -n CORES`
pytest-xdist