#!/usr/bin/env python
from setuptools import setup

# Allow editable install into user site directory.
# See https://github.com/pypa/pip/issues/7953.
setup()
